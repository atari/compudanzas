#!/bin/sh

mkdir -p web/static
mkdir -p gem
mkdir -p tmp


fecha=$(date --rfc-3339=date)

echo "empezando conversión, estamos en ${fecha}"

echo "revisando y copiando imágenes..."
for f in $(find src/ -regextype awk -regex ".*(jpg|png|gif|svg)")
do
	path="web/${f#src/}" # quita el prefijo de src/ y agrega web/
	gempath="gem/${f#src/}" # quita el prefijo de src/ y agrega gem/
	mkdir -p $(dirname $path) # crea directorio si no existe
	mkdir -p $(dirname $gempath) # crea directorio si no existe
	cp -vu $f $path
	cp -vu $f $gempath
done

cp src/tw.txt web/
cp src/tw.txt gem/

#cp src/llave_sejo.asc web/
#cp src/llave_sejo.asc gem/

cp src/atom.xml web/
cp src/atom.xml gem/
cp src/estilo.css web/static/

# limpia
echo "limpiando archivos previos..."

rm web/*html
rm gem/*gmi
rm tmp/*gmo

# crea índice
echo "generando índice..."
rm src/pages.gmo
ls -t src/*gmo | awk -f genindice.awk > src/pages.gmo

# genera incoming links en tmp/
echo "generando incoming links..."
python3 links.py

echo "revisando y convirtiendo archivos gmo a html y gmi..."

i=0
# convierte y actualiza archivos gmi a html
for f in $(find tmp/ -iname *.gmo)
do

	path=${f#tmp/} # quita el prefijo de "tmp/"
	htmlpath="web/${path%gmo}html" # agrega "web/" y cambia el sufijo
	gempath="gem/${path%gmo}gmi" # agrega "gem/"

#	fecha=$(date -r $f --rfc-3339=date)
	fecha=$(date -r src/$path --rfc-3339=date)


#	echo "${f}"
#		echo "${f} -> ${htmlpath}"
#		echo "${f} -> ${gempath}"
	# haz la conversión
	awk -v fecha=$fecha -f gemtext2html.awk $f > $htmlpath
	awk -v fecha=$fecha -f gmo2gmi.awk $f   > $gempath

	(( i++ ))

done

cp web/home.html web/index.html
cp gem/home.gmi gem/index.gmi

echo "convertidas ${i} páginas!"
