# compudanzas

this repository contains the scripts and source files for the compudanzas site in gemini and the web

* generated in web/ : [https://compudanzas.net](https://compudanzas.net)
* generated in gem/ : [gemini://compudanzas.net](gemini://compudanzas.net/)

# generate site

call the shell script:

```
./generasitio.sh
```

# files

* `generasitio.sh`: the shell script that copies files and call the other scripts with the corresponding arguments (like last modified date)
* `gemtext2html.awk`: converts the files in .gmo format (modified .gmi) to an html version, according to the spec and my own taste
* `gmo2gmi.awk`: converts the files in .gmo format to real .gmi
* `genindice.awk`: generates the index of all pages
* `links.py`: calculates the incoming links between pages and adds them to the files

`gemtext2html.awk` and `gmo2gmi.awk` also parse and convert {wikilinks}

# .gmo format

the same format as gmi (gemtext), but with the following line types:

`+` add this line to the html file but not to the gmi file

`&` add this line to the gmi file but not to the html file

# wikilinks

## inline wikilinks

the generator can parse and convert inline {wikilinks}, one per line

* in html, it gets converted to an inline link
* in gemtext, a link is added after the line with the link

.gmo text:

```
see the {roadmap} for updates that want to happen.
```

converted to html:

```html
<p>
see the <a href='./roadmap.html'>{roadmap}</a> for updates that want to happen.
</p>
```

converted to gemtext:

```gemtext
see the {roadmap} for updates that want to happen.
=> ./roadmap.gmi {roadmap}
```

spaces inside the wikilinks are converted to underscores.

## gemtext-like wikilinks

the generator can also parse normal links in gemtext, one per line

* in html, it gets converted to an inline link
* in gemtext, the link is left as is

.gmo text:

```
=> ./references.gmi {references}
```

converted to html:

```html
<p><a href='./references.html'>{references}</a></p>
```

converted to gemtext:

```
=> ./references.gmi {references}
```

## incoming links calculation

`links.py` takes both of those types of wikilinks in order to perform its calculation.

it looks at the contents between the curly brackets, converts them to a filename format (replacing spaces for underscores), and assummes that file actually exists.
