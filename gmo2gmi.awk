function nombre2Link( t, r ){ # convierte un nombre con espacios, a uno con r (e.g. "_"
	gsub(" ",r,t);
	return t
}

/^+ /{ # literal html
	next # salta la línea
}

/^& /{ # literal gemtext
	sub(/^& /,"",$0)
	print $0
	next

}

# imprime líneas normamlmente

{ 
	print $0
}

# si tienen sintaxis de wikilink, y no son líneas de enlace, agrega el link
# (el % es por las líneas de uxn jaja)
/^[^=%].+{.+}/{
	i = match( $0, /{.+}/)
	ifinal = index($0, "}") # índice del } final

	link = substr($0, i, ifinal-i+1) # {link}
	nombre = substr($0, i+1, ifinal-i-1) # link	
	nombre = nombre2Link( nombre, "_" )

	print "=> ./" nombre ".gmi " link 
}

# agrega footer 

END{
	print ""
	print ""
	sub("tmp/","",FILENAME)
	if(FILENAME != "home.gmo"){
		print "=> https://codeberg.org/sejo/compudanzas/src/branch/main/src/" FILENAME " source file"
		print "most recent update on: 1" fecha 
		print "=> ./home.gmi {compudanzas}"
	}
	print "=> https://wiki.p2pfoundation.net/Peer_Production_License peer production license" 
}
