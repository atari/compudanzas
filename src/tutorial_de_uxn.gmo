# tutorial de uxn

una guía de inicio para programar la computadora varvara basada en el núcleo {uxn}, y un pausado complemento a la documentación oficial.

=> https://wiki.xxiivv.com/site/uxntal.html uxntal
=> https://wiki.xxiivv.com/site/uxnemu.html uxnemu

el tutorial está dividido en 8 días (o secciones), ya que puede ser seguido junto al taller.

(al día de hoy, este es un trabajo en proceso)


# día 1

en esta primera sección del tutorial vamos a hablar de los aspectos básicos de la computadora uxn llamada varvara, su paradigma de programación, su arquitectura, y por qué podrías queres aprender a programar en ella.

también vamos a saltar directo a nuestros primeros programas simples para demostrar conceptos fundamentales que desarrollaremos en los días siguientes.

{tutorial de uxn día 1}

# día 2

en esta sección vamos a empezar a explorar los aspectos visuales de la computadora varvara: ¡hablamos sobre los aspectos fundamentales del dispositivo de pantalla para que podamos empezar a dibujar en ella!
también discutiremos el trabajo con cortos (2 bytes) además de los números de un sólo byte en uxntal.

{tutorial de uxn día 2}


# día 3

aquí introducimos el uso del dispositivo controlador en la computadora varvara: esto nos permite agregar interactividad a nuestros programas, y empezar a implementar control de flujo en uxntal.

también hablamos de instrucciones lógicas y de manipulación de la pila en uxntal.

{tutorial de uxn día 3}

# día 4

¡proximamente!

# índice tentativo

este índice está aquí y ahora como referencia de la estructura general del tutorial.

## día 1: aspectos básicos

* ¿por qué uxn?
* notación {postfix}
* arquitectura de la computadora uxn
* instalación y toolchain
* un hola mundo muy básico
* etiquetas, macros y runas
* un hola mundo mejorado
* imprimir un dígito

instrucciones nuevas: LIT, DEO, ADD, SUB

=> https://git.sr.ht/~rabbits/uxn/ uxn repo

## día 2: la pantalla

* modo corto
* colores del sistema
* dibujar pixeles
* sprites: formato chr, nasu
* dibujar sprites sprites
* operaciones en la pila
* practica: reoetición manual de un sprite

instructiones nuevas: DEI, MUL, DIV, SWP, OVR, ROT, DUP, POP

modo nuevo: modo corto

=> https://wiki.xxiivv.com/site/nasu.html nasu

## día 3: interactividad con el teclado

* vector de controlador
* control de flujo: condicionales, saltos relativos y absolutos
* runas para direcciones
* botón y tecla
* máscaras bitwise
* práctica: mover/cambiar sprite con el teclado

instrucciones nuevas: EQU, NEQ, JCN, JMP, AND, ORA, EOR, SFT

## día 4: bucles y animación

* control de flujo: repetición de un sprite
* vector de pantalla
* variables: página cero, relativas, absolutas
* offsets en direcciones
* timing de animación
* práctica: sprite animado

instrucciones nuevas: LTH, GTH, STZ, STR, STA, LDZ, LDR, LDA

## día 5: interactividad con el mouse

* dispositivo y vector del mouse
* Pila de retorno y modo
* subrutinas: parametros, llamada y retorno
* práctica: sprite como puntero
* práctica: herramienta de dibujo simple

instrucciones nuevas: STH, JSR 

modo nuevo: modo de retorno

## día 6: audio

* el dispositivo de audio
* samples como sprites de audio
* adsr
* pitch
* práctica: pequeño instrumento musical

## día 7: modo de retención y otros dispositivos

* modo de retención
* re-escribiendo código con modo de retención
* dispositivo de archivo: guardando y cargando un estado simple
* dispositivo de fecha y hora: leyendo fecha y hora
* práctica: visualización de fecha y hora

modo nuevo: modo de retención

## día 8: tiempo de demo

* comparte lo que has creado :)

# soporte

si este tutorial te ha sido de ayuda, considera compartirli y brindarle tu {apoyo} :)
